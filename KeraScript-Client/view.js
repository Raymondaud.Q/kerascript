let cnv;
let model;
let c;
let randomBatch;
let matrix = randomBatchInput(1,100)
let size = 200;
let nbScarePerLine = 10
let scaledSize = size/nbScarePerLine

function setup() {
	cnv = createCanvas(size, size);
	cnv.position(210,100)
	c = document.getElementById("myCanvas");
	for (let element of document.getElementsByClassName("p5Canvas")) {
		element.addEventListener("contextmenu", (e) => e.preventDefault());
	}
	matrix = randomBatchInput(1,100)
}

function nullInput(){
	matrix = nullBatchInput(1,100)
}


function draw() {
	background(220);
	if ( outputtensor != null )
		tf.browser.toPixels(outputtensor, c);
	for ( let i = 0 ; i < nbScarePerLine ; i++ ){
		for ( let j  = 0 ; j < nbScarePerLine ; j++ ){
			let val = (matrix[0][j*nbScarePerLine+i]*255)
			fill( -val, 0, val )
			square( scaledSize * i, scaledSize * j, scaledSize)
		}
	}
}

function mousePressed() {
	column =  Math.floor(mouseY/scaledSize) % 10
	line = Math.floor(mouseX/scaledSize) % 10

	if ( mouseButton == LEFT )
		matrix[0][(column*nbScarePerLine)+line] += 0.1
	else if ( mouseButton == RIGHT )
		matrix[0][(column*nbScarePerLine)+line] -= 0.1
}

function mouseDragged() {
	column =  Math.floor(mouseY/scaledSize) % 10
	line = Math.floor(mouseX/scaledSize) % 10

	if ( mouseButton == LEFT )
		matrix[0][(column*nbScarePerLine)+line] += 0.1
	else if ( mouseButton == RIGHT )
		matrix[0][(column*nbScarePerLine)+line] -= 0.1
}