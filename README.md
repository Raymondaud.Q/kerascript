
[Test the release on itch.io ](https://quentinraymondaud.itch.io/ai-in-web)

# Convert Keras model to TensorflowJS model

[Youtube : TFJS Startup](https://www.youtube.com/watch?v=f8ed_xAjkOg)

You need : 
* pip3 install tensorflowjs
* [A Keras Model](https://gitlab.com/uapv-ceri-m1-projet-groupe3/ml-gan-emojis)

Usage :

```
tensorflowjs_converter --input_format keras \
path/to/model.h5 \
path/to/dest/dir \
```

## Dependencies

- [P5js](https://p5js.org/)
- [ExpressJs](https://expressjs.com/)
- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Dependencies that you must install

- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Build & Run the projet :

- Download this rep
- Extract Files
- Fill IPORT  in "/client/main.js" with the server address 
- Run `npm i`
- Run `node server.js`
- Go to localhost:3000 ( or whatever you've configured in server.js & TensorFlowScript.js )
- Enjoy

![User Interface](https://gitlab.com/Raymondaud.Q/kerascript/raw/master/screenshot.gif "Demo")