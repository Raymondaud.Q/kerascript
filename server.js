const express = require('express')
const app = express()
const port = 3000

app.use(express.static(__dirname + '/KeraScript-Client/'));
app.get('/', (req, res) => {
	console.log("Routed")
	res.sendFile('index.html', {root: __dirname })
})

app.use('/disc', express.static('tfjs_discriminator'))
app.use('/gen', express.static('tfjs_generator'))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})
